#include "player.h"
#include <cstdio>
#include <vector>
//Uma has edited Player.cpp :)
//attempt 2

//small change test

#define DEPTH   2


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    this->side = side;
    std::cerr << "Our side is " << side << std::endl;
    
    this->board = new Board;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

void Player::set_board(Board* board_state)
{
    this->board = board_state->copy();
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    Side other = (side == BLACK) ? WHITE : BLACK;
    board->doMove(opponentsMove, other);
    
    if(!(this->board->hasMoves(this->side)))
    {
        std::cerr << "No moves, returning null..." << std::endl;
        return NULL;
    }    
    
    int score;
    int max = -1000;
    Board* temp_board;
    Move* move;
    std::vector<Move*> moves;
    
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            move = new Move(i, j);
            if(board->checkMove(move, this->side))
            {
                moves.push_back(move);
            }
        }
    }
    
    Move* next_move;
    if(moves.size() == 1)
    {
        return moves[0];
    }
    else
    {
        for(unsigned int n = 0; n < moves.size(); n++)
        {
            temp_board = board->copy();
            temp_board->doMove(moves[n], this->side);
            score = minimax(side, temp_board, 4, -1000, 1000);
            if (score > max)
            {
                next_move = moves[n];
                max = score;
            }
        }
    }
    board->doMove(next_move, this->side);
    return next_move;
}


int Player::minimax(Side sides, Board* board, int depth, int alpha, int beta)
{    
    if(depth == 0)
    {
        return (board->Counter(side));
    }
    else
        {
        Side other = (side == BLACK) ? WHITE : BLACK;
        Board* temp_board;
        int score;
        std::vector<Move*> moves;
        Move* move;
        if(sides == this->side)
        {
            for(int k = 0; k < 8; k++)
            {
                for(int j = 0; j < 8; j++)
                {
                    move = new Move(k, j);
                    if(board->checkMove(move, side))
                    {
                        moves.push_back(move);
                    }
                }
            }
            if (moves.size() == 0)
            {
                temp_board = board;
                return minimax(other, temp_board, depth - 1, alpha, beta) - 2;
            }
            for(unsigned i = 0; i < moves.size(); i++)
            {
                temp_board = board;
                temp_board->doMove(moves[i], side);
                score = minimax(other, temp_board, depth - 1, alpha, beta);
                if(score > alpha)
                {
                    alpha = score;
                    if (beta < alpha)
                    {
                        return -10000;
                    }
                }
            }
            return alpha;
        }
        else if (sides != this->side)
        {
            for(int k = 0; k < 8; k++)
            {
                for(int j = 0; j < 8; j++)
                {
                    move = new Move(k, j);
                    if(board->checkMove(move, other))
                    {
                        moves.push_back(move);
                    }
                }
            }
            if (moves.size() == 0)
            {
                temp_board = board;
                return minimax(side, temp_board, depth - 1, alpha, beta) + 2;
            }
            for(unsigned i = 0; i < moves.size(); i++)
            {
                temp_board = board;
                temp_board->doMove(moves[i], side);
                score = minimax(side, temp_board, depth - 1, alpha, beta);
                if(score < beta)
                {
                    beta = score;
                }
            }
                return beta;
        }
        else
        {
            return -100;
        }
    }
}
    

Move *Player::Heuristic(std::vector<Move *> moves, Board* temp_board)
{
    Move* next_move = moves[0];
    int score = 0;
    int max_score = -100;
    Side other = (side == BLACK) ? WHITE : BLACK;
    
    for(unsigned i = 0; i < moves.size(); i++)
    {
        temp_board->doMove(moves[i], this->side);
        score = temp_board->count(side) - temp_board->count(other);
        if(score > max_score)
        {
            max_score = score;
            next_move = moves[i];
        }
    }
    return next_move;
}
