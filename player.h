#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void set_board(Board* board_state);
    
    bool testingMinimax;
    Side side;
    
private:
    Board *board;
    Move* Heuristic(std::vector<Move*> moves, Board* temp_board);
    int minimax(Side sides, Board* board, int depth, int alpha, int beta);
};

#endif
